package main

import (
	"flag"
	"os"
)

// Variables used for command line parameters
var (
	Config struct {
		Token         string
		MongoUri      string
		MongoDatabase string
		FfURL         string
		FfInstanceId  string
		FfEnv         string
	}
)

func init() {
	// usage of the command line parameters
	flag.StringVar(&Config.Token, "t", "", "Bot Token")
	flag.StringVar(&Config.MongoUri, "m", "", "MongoDB URI")
	flag.StringVar(&Config.MongoDatabase, "d", "gtg-dev", "MongoDB Database")
	flag.Parse()

	// override with env variables
	if token := os.Getenv("TOKEN"); token != "" {
		Config.Token = token
	}
	if mongoUri := os.Getenv("MONGO_URI"); mongoUri != "" {
		Config.MongoUri = mongoUri
	}
	if mongoDatabase := os.Getenv("MONGO_DATABASE"); mongoDatabase != "" {
		Config.MongoDatabase = mongoDatabase
	}

	// only with environment variables
	if ffUrl := os.Getenv("FF_URL"); ffUrl != "" {
		Config.FfURL = ffUrl
	}
	if ffInstanceId := os.Getenv("FF_INSTANCE_ID"); ffInstanceId != "" {
		Config.FfInstanceId = ffInstanceId
	}
	if ffEnv := os.Getenv("FF_ENV"); ffEnv != "" {
		Config.FfEnv = ffEnv
	}
}
