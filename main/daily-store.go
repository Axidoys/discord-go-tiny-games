package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

// document naming convention: daily-<gameType>-<date>
// contains a list of {player, score} for the day in a variable named dailyScoreboard

type DailyScore struct {
	Player string
	Score  int
}

// DailyScore represents the daily scores for a specific game type.
type DailyScoreboard struct {
	// Use bson tags to specify the MongoDB field name.
	DailyScoreboard []DailyScore `bson:"dailyScoreboard"`
}

func updateDaily(gameResult Result, timestamp time.Time) {
	client, _ := login()

	franceTimeZone, err := time.LoadLocation("Europe/Paris")
	if err != nil {
		fmt.Println("Error loading time zone:", err)
		panic(err)
	}

	formattedDate := timestamp.In(franceTimeZone).Format("2006-01-02") // so strange how they have defined the layout
	documentId := "daily-" + string(gameResult.GameType) + "-" + formattedDate

	// find a document for today
	collection := client.Database(Config.MongoDatabase).Collection("daily")
	queryResult := collection.FindOne(context.Background(), bson.D{{"_id", documentId}})

	// if not found, create one
	if queryResult.Err() == mongo.ErrNoDocuments {
		collection.InsertOne(context.Background(), bson.D{{"_id", documentId}})
	}

	// update the document with the new result
	score := gameResult.TotalGuesses - gameResult.Guesses
	if gameResult.TotalGuesses == -1 {
		score = 0
	}
	updateResult, err := collection.UpdateOne(context.Background(),
		bson.D{{"_id", documentId}},
		bson.D{{"$push", bson.D{{"dailyScoreboard", bson.D{{"player", gameResult.Player.Pseudo}, {"score", score}}}}}})

	if err != nil {
		panic(err)
	}

	fmt.Printf("Updated document with _id: %v\n", updateResult.UpsertedID)
}

func retrieveDaily(gameType string) []DailyScore {
	client, _ := login()

	currentDate := time.Now().UTC()
	formattedDate := currentDate.Format("2006-01-02") // so strange how they have defined the layout
	documentId := "daily-" + string(gameType) + "-" + formattedDate

	// find a document for today
	collection := client.Database(Config.MongoDatabase).Collection("daily")
	queryResult := collection.FindOne(context.Background(), bson.D{{"_id", documentId}})

	// if not found, create one
	if queryResult.Err() == mongo.ErrNoDocuments {
		return nil //TODO is it an empty list ???
	}

	// get the dailyScoreboard from the queryResult
	// Get the dailyScoreboard from the queryResult
	var dailyScoreboard DailyScoreboard
	if err := queryResult.Decode(&dailyScoreboard); err != nil {
		fmt.Println("Error decoding dailyScoreboard:", err)
		return nil // Return an empty slice or handle the error accordingly.
	}

	return dailyScoreboard.DailyScoreboard
}

// TODO get list of players (number of days is a parameter)
// fetch all the daily-<gameType>-<date> documents from the last numberOfHistoryDays
// and get the list of players from the dailyScoreboard
func retrieveActivePlayers(gameType GameType, numberOfHistoryDays int) []string {
	client, _ := login()
	collection := client.Database(Config.MongoDatabase).Collection("daily")

	playerList := make([]string, 0)

	for dayCursors := 0; dayCursors < numberOfHistoryDays; dayCursors++ {
		currentDate := time.Now().UTC().AddDate(0, 0, -dayCursors)
		formattedDate := currentDate.Format("2006-01-02") // so strange how they have defined the layout
		documentId := "daily-" + string(gameType) + "-" + formattedDate

		queryResult := collection.FindOne(context.Background(), bson.D{{"_id", documentId}})

		// if not found, create one
		if queryResult.Err() == mongo.ErrNoDocuments {
			continue
		}

		// Get the dailyScoreboard from the queryResult
		var dailyScoreboard DailyScoreboard
		if err := queryResult.Decode(&dailyScoreboard); err != nil {
			fmt.Println("Error decoding dailyScoreboard:", err)
			continue
		}

		// add the players to the list
		for _, dailyScore := range dailyScoreboard.DailyScoreboard {
			// don't add the player if already in the list
			if !contains(playerList, dailyScore.Player) {
				playerList = append(playerList, dailyScore.Player)
			}
		}
	}

	return playerList
}

func contains(list []string, player string) bool {
	for _, pseudo := range list {
		if pseudo == player {
			return true
		}
	}
	return false
}

func retrieveRemainingActivePlayers(gameType GameType, numberOfHistoryDays int) []string {
	client, _ := login()
	collection := client.Database(Config.MongoDatabase).Collection("daily")

	currentDate := time.Now().UTC()
	formattedDate := currentDate.Format("2006-01-02") // so strange how they have defined the layout
	documentId := "daily-" + string(gameType) + "-" + formattedDate

	queryResult := collection.FindOne(context.Background(), bson.D{{"_id", documentId}})

	listOfAlreadyPlayedToday := make([]string, 0)

	// fill in listOfAlreadyPlayedToday with the queryResult
	var dailyScoreboard DailyScoreboard
	if err := queryResult.Decode(&dailyScoreboard); err != nil {
		fmt.Println("Error decoding dailyScoreboard:", err)
	}

	for _, dailyScore := range dailyScoreboard.DailyScoreboard {
		listOfAlreadyPlayedToday = append(listOfAlreadyPlayedToday, dailyScore.Player)
	}

	// get the list of active players
	listOfActivePlayers := retrieveActivePlayers(gameType, numberOfHistoryDays)

	// remove the players that have already played today
	for _, player := range listOfAlreadyPlayedToday {
		listOfActivePlayers = remove(listOfActivePlayers, player)
	}

	return listOfActivePlayers
}

func remove(players []string, player string) []string {
	for i, pseudo := range players {
		if pseudo == player {
			players[i] = players[len(players)-1]
			return players[:len(players)-1]
		}
	}
	return players
}
