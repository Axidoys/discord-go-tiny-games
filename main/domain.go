package main

type Player struct {
	Id     string
	Pseudo string
}

type GameType string

var (
	WordleGameType      GameType = "Wordle"
	LouanGameType       GameType = "Louan"
	ConnectionsGameType GameType = "Connections"

	// list to be maintained for validation
	GameTypes = []GameType{WordleGameType, LouanGameType, ConnectionsGameType}
)

type Result struct {
	GameType     GameType
	GameId       int
	Player       Player
	Guesses      int // TODO for now -1 if failed
	TotalGuesses int
}

func isValidGameType(gameType string) bool {
	for _, validGameType := range GameTypes {
		if gameType == string(validGameType) {
			return true
		}
	}
	return false
}
