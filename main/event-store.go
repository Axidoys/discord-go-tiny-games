package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
)

func insertResult(gameResult Result) {
	client, _ := login()

	collection := client.Database(Config.MongoDatabase).Collection("event-store")
	queryResult, err := collection.InsertOne(context.TODO(), gameResult)

	if err != nil {
		panic(err)
	}

	fmt.Printf("Inserted document with _id: %v\n", queryResult.InsertedID)
}

func retrieveResults(gameType string) (results []Result) {
	filter := bson.D{{"gametype", gameType}}

	client, _ := login()
	collection := client.Database(Config.MongoDatabase).Collection("event-store")
	cursor, err := collection.Find(context.Background(), filter)

	if err != nil {
		panic(err)
	}

	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		var result Result
		//TODO better handle error
		// may should continue if one doc is faulty
		// need to make sure that if one doc is faulty, it does not land in the results
		if err := cursor.Decode(&result); err != nil {
			panic(err)
		}
		results = append(results, result)
	}

	if err := cursor.Err(); err != nil {
		panic(err)
	}

	return results
}
