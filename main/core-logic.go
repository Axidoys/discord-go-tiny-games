package main

import (
	"github.com/Unleash/unleash-client-go/v3"
	"github.com/bwmarrin/discordgo"
	"log"
	"sort"
	"strconv"
	"strings"
)

type PlayerScore struct {
	Pseudo string
	Score  int
}

type Scoreboard []PlayerScore

func handleMessageForGameResults(s *discordgo.Session, m *discordgo.MessageCreate) {
	result := resultParsing(m)

	if result == nil {
		return
	}

	log.Println("Result: ", result)

	// save the result in the event store database
	insertResult(*result)

	// save the result in the projected score database
	//TODO really ? or not needed ?

	// summary message
	summaryLine := ""
	if unleash.IsEnabled("react-summary") {
		g := strconv.Itoa(result.Guesses)
		if result.Guesses == -1 {
			g = "X"
		}
		summaryLine = "***" + result.Player.Pseudo + " sur " + string(result.GameType) + " : " + g + "/" + strconv.Itoa(result.TotalGuesses) + "***" + "\n"
	}

	// reaction message
	resultReactLine := ""
	if unleash.IsEnabled("react-result") {
		resultReactLine = reactionMessage(*result) + "\n"
	}

	// update daily score
	updateDaily(*result, m.Timestamp)

	// cta message
	cta := ""
	if unleash.IsEnabled("cta") {
		toBeCalledPlayers := retrieveRemainingActivePlayers(result.GameType, 5)

		if len(toBeCalledPlayers) > 0 {
			cta = "\nAllez, reste à jouer : " + strings.Join(toBeCalledPlayers, ", ") + " !"
		} else {
			cta = "\nTout le monde a joué, coool !"
			// TODO voici le scoreboard
		}
	}

	s.ChannelMessageSend(m.ChannelID, summaryLine+resultReactLine+cta)
	return
}

func commandScoreboard(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Content == "gtg-scoreboard" {
		s.ChannelMessageSend(m.ChannelID, "Please specify a game type.")
		return
	}

	gameType := strings.TrimPrefix(m.Content, "gtg-scoreboard ")
	// check if the game type is valid
	if !isValidGameType(gameType) {
		s.ChannelMessageSend(m.ChannelID, "Game type "+gameType+" is not valid.")
		return
	}

	results := retrieveResults(gameType)
	scoreboard := createScoreboard(results)

	//create message
	message := "Scoreboard:\n"
	for _, playerScore := range scoreboard {
		message += playerScore.Pseudo + " " + strconv.Itoa(playerScore.Score) + "\n"
	}

	//send message
	s.ChannelMessageSend(m.ChannelID, message)
}

func commandDaily(s *discordgo.Session, m *discordgo.MessageCreate) {
	// TODO duplicate code with the commandScoreboard function
	if m.Content == "gtg-daily" {
		s.ChannelMessageSend(m.ChannelID, "Please specify a game type.")
		return
	}

	gameType := strings.TrimPrefix(m.Content, "gtg-daily ")
	// check if the game type is valid
	if !isValidGameType(gameType) {
		s.ChannelMessageSend(m.ChannelID, "Game type "+gameType+" is not valid.")
		return
	}

	dailyScoreboard := retrieveDaily(gameType)

	//create message
	message := "Daily scoreboard:\n"
	for _, dailyScore := range dailyScoreboard {
		message += dailyScore.Player + " " + strconv.Itoa(dailyScore.Score) + "\n"
	}

	//send message
	s.ChannelMessageSend(m.ChannelID, message)
}

func createScoreboard(results []Result) Scoreboard {
	scoreboard := Scoreboard{}
	playerScores := make(map[string]int)

	for _, result := range results {
		pseudo := result.Player.Pseudo
		score := result.TotalGuesses - result.Guesses
		if result.Guesses == -1 {
			score = 0
		}

		// Check if the player is already in the map.
		if existingScore, found := playerScores[pseudo]; found {
			// If the player is in the map, update their score.
			playerScores[pseudo] = existingScore + score
		} else {
			// Otherwise, add the player to the map with their score.
			playerScores[pseudo] = score
		}
	}

	// Convert the map to a Scoreboard.
	for pseudo, score := range playerScores {
		scoreboard = append(scoreboard, PlayerScore{Pseudo: pseudo, Score: score})
	}

	// Sort the scoreboard in descending order based on scores.
	sort.Slice(scoreboard, func(i, j int) bool {
		return scoreboard[i].Score > scoreboard[j].Score
	})

	return scoreboard
}
