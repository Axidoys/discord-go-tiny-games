package main

import (
	"github.com/bwmarrin/discordgo"
	"strings"
)

func discordMessageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	handleMessageForGameResults(s, m)

	handleCommands(s, m)
}

func handleCommands(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, "gtg-scoreboard") {
		commandScoreboard(s, m)
	}

	if strings.HasPrefix(m.Content, "gtg-daily") {
		commandDaily(s, m)
	}
}
