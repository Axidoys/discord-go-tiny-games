package main

import (
	"github.com/Unleash/unleash-client-go/v3"
)

type metricsInterface struct {
}

func featureFlagInit() {
	unleash.Initialize(
		unleash.WithUrl(Config.FfURL),
		unleash.WithInstanceId(Config.FfInstanceId),
		unleash.WithAppName(Config.FfEnv),
		unleash.WithListener(&metricsInterface{}),
	)
}
