package main

import (
	"github.com/bwmarrin/discordgo"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var rWordle, _ = regexp.Compile("Wordle ([0-9]+) ([X0-9]+)/([0-9]+)")
var rLouan, _ = regexp.Compile("Le Mot \\(@WordleFR\\) #([0-9]+) ([X0-9]+)/([0-9]+)")
var rConnectionsGameId, _ = regexp.Compile("Puzzle #([0-9]+)")

// enum  for the state of the parser
type ResultParserState int

const (
	// the parser is looking for the header
	LookingForHeader ResultParserState = iota
	// ...
	LookingForGameContent
	// the parser is done
	Done
)

type ResultParser struct {
	state        ResultParserState
	result       Result
	bufferMemory any
}

type BufferConnections struct {
	successfulGuesses int
	wrongGuesses      int
}

func resultParsing(m *discordgo.MessageCreate) *Result {
	initResultParser := ResultParser{
		state: LookingForHeader,
		// partially initialized result
		result: Result{Player: Player{Id: m.Author.ID, Pseudo: m.Author.Username}}}
	resultParser := initResultParser // TODO check if really a copy done ?

	for _, line := range strings.Split(m.Content, "\n") {
		if resultParser.state == LookingForGameContent {
			resultParser = lookingForGameContent(line, resultParser)
		}
		if resultParser.state == LookingForHeader {
			resultParser = lookingForHeader(line, resultParser)
		}

		// if succeeded
		if resultParser.state == Done {
			return &resultParser.result // TODO memory mgmt ?
		}
	}

	// TODO refactor to a generic function 🙏
	if resultParser.state == LookingForGameContent && resultParser.result.GameType == "Connections" {
		if resultParser.bufferMemory != nil && resultParser.bufferMemory != 0 {
			// case where the message finish with the gameContent/gameResult, can wrap up the parsing now then
			totalGuesses, guesses := fromBufferToPartialResults(resultParser.bufferMemory.(BufferConnections))

			resultParser.state = Done
			resultParser.result.TotalGuesses = totalGuesses
			resultParser.result.Guesses = guesses
			return &resultParser.result
		}
	}

	// return empty
	return nil
}

func lookingForHeader(line string, resultParser ResultParser) ResultParser {
	foundHeader, resultParser := lookingForHeaderWordle(line, resultParser)
	if foundHeader {
		return resultParser
	}

	foundHeader, resultParser = lookingForHeaderLouan(line, resultParser)
	if foundHeader {
		return resultParser
	}

	foundHeader, resultParser = lookingForHeaderConnections(line, resultParser)
	if foundHeader {
		return resultParser
	}

	return resultParser
}

func lookingForHeaderWordle(line string, resultParser ResultParser) (bool, ResultParser) {
	matches := rWordle.FindStringSubmatch(line)
	if matches != nil && len(matches) == 4 {

		id, guesses, totalGuesses := readWordleLikeHeaderMatches(matches)

		resultParser.state = Done
		resultParser.result.GameType = WordleGameType
		resultParser.result.GameId = id
		resultParser.result.Guesses = guesses
		resultParser.result.TotalGuesses = totalGuesses
		return true, resultParser
	}
	return false, resultParser
}

func lookingForHeaderLouan(line string, resultParser ResultParser) (bool, ResultParser) {
	matches := rLouan.FindStringSubmatch(line)
	if matches != nil && len(matches) == 4 {

		id, guesses, totalGuesses := readWordleLikeHeaderMatches(matches)

		resultParser.state = Done
		resultParser.result.GameType = LouanGameType
		resultParser.result.GameId = id
		resultParser.result.Guesses = guesses
		resultParser.result.TotalGuesses = totalGuesses
		return true, resultParser
	}
	return false, resultParser
}

func lookingForHeaderConnections(line string, resultParser ResultParser) (bool, ResultParser) {
	if line == "Connections " || line == "Connections" {
		resultParser.state = LookingForGameContent
		resultParser.result.GameType = ConnectionsGameType
		return true, resultParser
	}

	return false, resultParser
}

func lookingForGameContent(line string, resultParser ResultParser) ResultParser {
	resultingParser, err := lookingForGameContentSafe(line, resultParser)
	if err != "" { // shortcut rule of the fsm
		log.Println(err)
		return resultingParser
	}
	return resultingParser
}

func lookingForGameContentSafe(line string, resultParser ResultParser) (ResultParser, string) {
	if resultParser.result.GameType == ConnectionsGameType {
		return lookingForGameContentConnections(line, resultParser)
	} else {
		return resultParser, "Game type not implemented for content"
	}
}

func lookingForGameContentConnections(line string, resultParser ResultParser) (ResultParser, string) {
	if resultParser.bufferMemory == nil {
		matches := rConnectionsGameId.FindStringSubmatch(line)
		if matches != nil && len(matches) == 2 {
			id, _ := strconv.Atoi(matches[1])
			resultParser.result.GameId = id
			resultParser.bufferMemory = BufferConnections{0, 0} // contains the number of guesses
			return resultParser, ""
		} else {
			return resultParser, "Was expecting a game id"
		}
	} else {
		okEmoji := "🟨🟩🟦🟪🟥"

		if strings.ContainsAny(line, okEmoji) {
			if successEmojiLine(line) {
				resultParser.bufferMemory =
					BufferConnections{
						successfulGuesses: resultParser.bufferMemory.(BufferConnections).successfulGuesses + 1,
						wrongGuesses:      resultParser.bufferMemory.(BufferConnections).wrongGuesses,
					}
			} else {
				resultParser.bufferMemory =
					BufferConnections{
						successfulGuesses: resultParser.bufferMemory.(BufferConnections).successfulGuesses,
						wrongGuesses:      resultParser.bufferMemory.(BufferConnections).wrongGuesses + 1,
					}
			}
			return resultParser, ""
		} else {
			totalGuesses, guesses := fromBufferToPartialResults(resultParser.bufferMemory.(BufferConnections))

			resultParser.result.TotalGuesses = totalGuesses
			resultParser.result.Guesses = guesses
			resultParser.state = Done
			return resultParser, ""
		}
	}
}

/* Support functions */

func readWordleLikeHeaderMatches(matches []string) (int, int, int) {
	id, _ := strconv.Atoi(matches[1])

	guesses := -2
	if matches[2] == "X" {
		guesses = -1
	} else {
		guesses, _ = strconv.Atoi(matches[2])
	}
	totalGuesses, _ := strconv.Atoi(matches[3])
	return id, guesses, totalGuesses
}

func successEmojiLine(line string) bool {
	// when the line is a success, it is a repetition of the same character
	var firstChar rune

	for i, char := range line {
		if char != ' ' {
			if i == 0 {
				firstChar = char
			} else if char != firstChar {
				return false
			}
		}
	}

	return true
}

func fromBufferToPartialResults(connections BufferConnections) (int, int) {
	totalGuesses := 4
	guesses := connections.wrongGuesses
	if connections.successfulGuesses != 4 {
		guesses = -1
	}
	return totalGuesses, guesses
}
