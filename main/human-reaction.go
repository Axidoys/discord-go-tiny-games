package main

import (
	"math/rand"
	"strconv"
)

func reactionMessage(result Result) string {
	// Shortcuts for the result fields.
	pp := result.Player.Pseudo
	gt := string(result.GameType)
	g1 := strconv.Itoa(result.Guesses)
	//g2 := strconv.Itoa(result.TotalGuesses)
	//g3 := g1 + "/" + g2 + "essais"

	// Define arrays of strings for each scale.
	perfectScale := []string{
		"Wow, ONE SHOT ! 🎯",
		"C'est un sans faute " + pp + " ! ✅",
		"Incroyable ! 🎊",
		"Tu maîtrises " + gt + " " + pp + " ! 🥷",
		"Tu es un vrai pro " + pp + " ! 😎",
		"Tu as été parfait " + pp + " !",
		"Absolument sensationnel, " + pp + " ! 🌟",
		"Un score époustouflant en " + gt + ", " + pp + " ! 🚀",
		"Tu es une légende " + pp + " ! 🏆",
		"Je suis bluffé par tes talents en " + gt + ", " + pp + " ! 🤯",
		"Une performance de haut niveau en " + gt + ", " + pp + " ! 🥇",
		"Tu as réussi un exploit extraordinaire en " + gt + ", " + pp + " ! 🌈",
		"Tu as atteint le sommet de " + gt + ", " + pp + " ! 🗻",
		"Ton score est simplement extraordinaire " + pp + " ! 🌟"}
	greatScale := []string{
		"Bravo " + pp,
		"Bien joué !",
		"C'est un sacré score, " + g1 + " essais !",
		"Quelques erreurs, mais un bon score dans l'ensemble.",
		"Tu es vraiment doué en " + gt + ", " + pp + " !",
		"Excellent ! Tu as bien joué sur " + gt + ".",
		"Génial " + pp + ", continue comme ça !",
		"Félicitations pour ce score élevé en " + gt + " !",
		"Un score impressionnant, " + pp + " !",
		"Tu as fait preuve d'une grande maîtrise en " + gt + "."}
	goodScale := []string{
		"Ca passe 🤏",
		"Limite...",
		"En s'entrainant un peu plus, tu peux faire mieux !",
		"Ne t'inquiètes pas, tu peux faire mieux la prochaine fois.",
		"Continue à t'entraîner en " + gt + ", tu progresses bien " + pp + " !",
		"Un peu d'effort supplémentaire et tu y arriveras " + pp + ".",
		"Tu es sur la bonne voie, continue à t'améliorer en " + gt + ".",
		"Ne lâche pas, avec de la persévérance tu atteindras de meilleurs scores " + pp + " !",
		"C'est bien, continue à t'exercer pour obtenir de meilleurs résultats.",
		"Chaque essai compte, tu vas t'améliorer en " + gt + " avec le temps " + pp + "."}
	lostScale := []string{
		"Dommage...",
		"Pas loin !",
		"Tu as essayé un peu au pif non ?",
		"Reprenons depuis le début...",
		"Reprends toi " + pp + " !",
		"💩",
		"🤡",
		"Boooooooh ... Booooh !",
		"C'est une stratégie originale, mais essayons autre chose " + pp + ".",
		"C'est tellement original que j'ai du mal à trouver les mots, " + pp + " !",
		"Tu as joué de manière audacieuse, mais ça n'a pas fonctionné cette fois.",
		"Bravo, tu as réussi à te démarquer avec ton approche unique en " + gt + ".",
		"Personne ne peut dire que tu n'es pas créatif " + pp + ", même si ça n'a pas marché.",
		"🤔 Il semblerait que nous soyons témoins d'une nouvelle stratégie révolutionnaire en " + gt + " !",
		"C'est comme si tu avais inventé une nouvelle façon de jouer à " + gt + " " + pp + " !"}

	ratio := float64(result.TotalGuesses-result.Guesses) / float64(result.TotalGuesses)

	switch {
	case result.Guesses == -1:
		return getRandomMessage(lostScale)
	case result.Guesses == 0:
		return getRandomMessage(perfectScale)
	case ratio >= 0.3:
		return getRandomMessage(greatScale) //+ " " + g3
	case ratio >= 0:
		return getRandomMessage(goodScale)
	default:
		return "👾" // placeholder message
	}
}

func getRandomMessage(messages []string) string {
	return messages[rand.Intn(len(messages))]
}
