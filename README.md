# Discord Go Tiny Games

Trying out Go with a small discord bot.

repo : https://gitlab.com/Axidoys/discord-go-tiny-games

## Set up GO environment

    go install github.com/bwmarrin/discordgo

Had to restart IJ few times.

## Set up Discord bot

1. Create a new application at https://discordapp.com/developers/applications/me
2. Take the token
3. tick the Message Content Intent (https://github.com/bwmarrin/discordgo/issues/961)
4. Go to https://discordapp.com/oauth2/authorize?client_id=YOUR_CLIENT_ID&scope=bot&permissions=0

## Run the bot

with

    -t <token>

